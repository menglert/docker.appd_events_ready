#!/bin/bash

echo -e "\x1B[7mLocation\x1B[27m"
read LOC

echo -e "\x1B[7mPortal User\x1B[27m"
read USER

echo -e "\x1B[7mPortal Password\x1B[27m"
read -s PASSWORD

echo -e "\x1B[7mType Version to Download (4.1.5.2 or latest)\x1B[27m"
read VERSION

echo -e "\x1B[7mDocker Image Name <user>/<repository>:<tag>\x1B[27m"
read IMG

cp $LOC/Dockerfile $LOC/Dockerfile.original

sed -i.bak "s|USER|$USER|g" $LOC/Dockerfile
sed -i.bak "s|PASSWORD|$PASSWORD|g" $LOC/Dockerfile
sed -i.bak "s|VERSION|$VERSION|g" $LOC/Dockerfile

docker build -t $IMG $LOC

mv $LOC/Dockerfile.original $LOC/Dockerfile
rm -rf $LOC/Dockerfile.bak

exit 0
