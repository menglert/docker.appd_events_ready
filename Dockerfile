FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ENV APPD_PLATFORM="/opt/appdynamics/platform"

ADD authorized_keys.pub /root/.ssh/authorized_keys
ADD start.sh ${APPD_PLATFORM}/

RUN apt-get update \
    && apt-get install -q -y openssh-server net-tools \
    && echo "ulimit -n 96000" > /etc/profile \
    && chmod -R 700 /root/.ssh \
    && chmod 600 /root/.ssh/authorized_keys \
    && chmod +x ${APPD_PLATFORM}/start.sh \
    && echo "vm.max_map_count = 262144" >> /etc/sysctl.conf \
    && echo "fs.file-max=819200" >> /etc/sysctl.conf

EXPOSE 22 9080

CMD [ "/bin/bash", "-c", "${APPD_PLATFORM}/start.sh" ]